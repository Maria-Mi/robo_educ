# Including main library binding
@include<'ur5'>() as Fragment

// moving to position
[0.0E-2, 0.0E-4, 0.73, 0.12, -1.93] when speed 1.2 when acc 1.4 as Circular
[0.0E-2, 4.0E-4, 0.44, 0.16, -1.66] when speed 1.2 when acc 1.4 as Linear
[0.0E-2, 0.1E-4, 0.38, 0.32, -2.13] when speed 0.6 when acc 0.2 as Joint

-- Including tools library
@include<'tools'>() as Fragment

// move tool
nt_move_tool(255)
nt_move_tool(0)

