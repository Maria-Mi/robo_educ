import React from "react";
import { Segment, Header } from "semantic-ui-react";
import Logo from "../Logo";
import json from "./../../../package.json";
export default () => (
    <Segment basic textAlign='center'>
        <Logo centered size='small' />
        <Header as='h1' textAlign='center'>
            <Header.Subheader>
                UR5 Phoenix v{json.version}
            </Header.Subheader>
        </Header>
    </Segment>
)