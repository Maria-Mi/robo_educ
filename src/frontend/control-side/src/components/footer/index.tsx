import * as React from "react";
import { List, Divider, Container, Segment, Image } from "semantic-ui-react";
import img from "./../../assets/logo.png";

export default () => (
    <Segment vertical>
        <Container textAlign='center'>
            <Divider section />
            <Image src={img} centered size='mini' />
            <br />
            <List horizontal divided link>
                <List.Item as='a' href='#'>
                    UR5 Phoenix {new Date().getFullYear()}
                </List.Item>
            </List>
        </Container>
    </Segment>
);