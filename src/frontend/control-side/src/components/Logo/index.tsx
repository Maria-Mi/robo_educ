import * as React from 'react'
import { Image } from 'semantic-ui-react'
import img from "./../../assets/logo.png";
export default props => <Image {...props} src={img} />