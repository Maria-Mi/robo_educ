import {URJointSector} from "./jointSector";
import {URToolPosition, URToolOrientation} from "./toolPosition";
export class URInfo
{
    // id: 0
    public timestamp: number;
    public physical: boolean;
    public real: boolean;
    public robotPowerOn: boolean;
    public emergencyStopped: boolean;
    public securityStopped: boolean;
    public programRunning: boolean;
    public programPaused: boolean;
    public mode: number;
    public robotMode: string;
    public speedFraction: number;
    //id: 1
    public sector: Array<URJointSector>;
    //id: 2
    public analogInputRange: Array<number>;
    public analogIn: Array<number>;
    public toolVoltage48V: number;
    public toolOutputVoltage: number;
    public toolCurrent: number;
    public toolTemperature: number;
    public toolMode: number;
    //id:3
    public masterTemperature: number;
    public robotVoltage48V: number;
    public robotCurrent: number;
    public masterIOCurrent: number;

    public masterSafetyState: number; // byte
    public masterOnOffState: number; // byte
    //id:4
    public ToolPosition: URToolPosition = new URToolPosition();
    public ToolOrientation: URToolOrientation = new URToolOrientation();

    //id:7
    public Dexterity: number;

    //id:8
    public teachButtonPressed: boolean;
    public teachButtonEnabled: boolean;
}