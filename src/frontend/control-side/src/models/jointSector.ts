export class URJointSector 
{
    public CodeID: number;

    public Mode: number;
    public MicroTemperature: number;
    public MotorTemperature: number;
    public Voltage: number;
    public JointCurrent: number;
    public JointTarger: number;
    public JointPosition: number = 0;
    public DegreePosition: number = 0;
    public JointSpeed: number;
    //id: 7 
    public force_mode_frame: number;
}