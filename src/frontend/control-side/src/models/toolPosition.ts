export class URToolPosition {
    public X: number;
    public Y: number;
    public Z: number;
}
export class URToolOrientation {
    public X: number;
    public Y: number;
    public Z: number;
}