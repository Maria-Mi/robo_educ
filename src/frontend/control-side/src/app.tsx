import * as React from "react";
import { Segment, Container } from "semantic-ui-react";
import AppHeader from "./components/AppHeader";
import Footer from "./components/Footer";
import RobotManager from "./components/Robot";
export default class App extends React.Component<{}, {}>
{
    render()  {
        return (
            <Container id='page' style={{ width: "60%", "min-width": "60%" }}>
                <Segment>
                    <AppHeader />
                    <RobotManager />
                    <Footer />
                </Segment>
            </Container>
        );
    }
}