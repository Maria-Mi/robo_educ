import React from "react";
import { Segment, Header, Button } from "semantic-ui-react";
import { Grid, Icon, SemanticICONS } from 'semantic-ui-react'

export type DrickProps = {name: string, icon: SemanticICONS, desc: string}

export default class Drink extends React.Component<DrickProps, any>
{
    render()
    {
        return (
            <Segment className="btn-2" textAlign='center'>
                <Header as='h2' icon>
                    {this.props.name}
                    <Icon name={this.props.icon} size='massive' />
                    <Header.Subheader>{this.props.desc}</Header.Subheader>
                </Header>
            </Segment>
        )
    }
}

// Moxito Blyat
// Лимон, кровь девственницы, кокс.