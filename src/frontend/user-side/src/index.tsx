import * as ReactDOM from "react-dom";
import * as React from "react";
import * as ReactGA from 'react-ga';
import App from "./app";
ReactGA.initialize('UA-70421575-6');
ReactGA.pageview(`${window.location.pathname}${window.location.search}`);


ReactDOM.render(
    <App />,
    document.getElementById('root') as HTMLElement
);