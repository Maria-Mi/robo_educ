﻿namespace urscript.interactive.exceptions
{
    using System;
    public class CompilationFailedException : Exception
    {
        private readonly string _message;
        private readonly string _thridMessage;
        private readonly string _fdmessage;

        public CompilationFailedException(string message, string thridMessage, string fdmessage)
        {
            _message = message;
            _thridMessage = thridMessage;
            _fdmessage = fdmessage;
        }

        public override string Message => $"\n{_message}\n{_thridMessage}\n{_fdmessage}";
    }
}