﻿namespace urscript.interactive
{
    using System.Collections.Generic;

    public class ParameterSyntax
    {
        public ParameterSyntax(string type, string identifier)
           : this(new TypeSyntax(type), identifier)
        {
        }

        public ParameterSyntax(TypeSyntax type, string identifier)
        {
            Type = type;
            Identifier = identifier;
        }


        public List<string> Modifiers { get; set; } = new List<string>();

        public TypeSyntax Type { get; set; }
        public string Identifier { get; set; }
    }
}