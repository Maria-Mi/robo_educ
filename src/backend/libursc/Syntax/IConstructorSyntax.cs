﻿namespace urscript.interactive
{
    using Sprache;

    public interface IConstructorSyntax
    {
        IResult Metadata { get; set; }
        string Construct();
    }
}