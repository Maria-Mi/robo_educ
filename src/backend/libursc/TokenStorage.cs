﻿namespace urscript.interactive
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using extensions;
    using Sprache;

    public static class TokenStorage
    {
        public static Parser<string> RawIdentifier =>
            from identifier in 
                Parse.Identifier(Parse.Letter.Or(Parse.Char('_').Token()), 
                    Parse.LetterOrDigit.Or(Parse.Char('_').Token()).Or(Parse.Char('-').Token()))
            where !URScriptWords.ReservedWords.Contains(identifier)
            select identifier;
        private static CommentParser CommentParser1 { get; } = new CommentParser("#" ,"/*", "*/", "\n");
        private static CommentParser CommentParser2 { get; } = new CommentParser("//" ,"/*", "*/", "\n");
        private static CommentParser CommentParser3 { get; } = new CommentParser("--" ,"/*", "*/", "\n");

        public static Parser<string> CommentParser =>
                    CommentParser1.AnyComment.Or(
                    CommentParser2.AnyComment).Or(
                    CommentParser3.AnyComment).
                Token().
                Named("comment");

        public static Parser<IEnumerable<string>> ArgsParameters(char op = '(', char cl = ')', char del = ',') =>
            from open in Parse.Char(op).Token()
            from args in 
                RawIdentifier.
                    Token().
                    Or(Parse.Regex(@"\-?[0-9]{1,}(\.[0-9]{1,})?", "a number").Token()).
                    DelimitedBy(Parse.Char(del).Token())
            from close in Parse.Char(cl).Token()
            select args;

        public static Parser<IEnumerable<string>> EmptyArgs(char op = '(', char cl = ')') =>
            from open in Parse.Char(op).Token()
            from close in Parse.Char(cl).Token()
            select new string[] { };



        public static Parser<TypeSyntax> SystemType =>
            Keyword(URScriptWords.Position).Or(
                Keyword(URScriptWords.Gate)).Or(
                Keyword(URScriptWords.Point)).Or(
                Keyword(URScriptWords.Void)).Or(
                Keyword(URScriptWords.Rotation)).Or(
                Keyword(URScriptWords.Vector)).
                Token().
                Select(x => new TypeSyntax(x)).
                Named("system_type");

        
        public static Parser<string> IdentifierToken =>
            RawIdentifier.Token().Named("u_identifier");


        public static Parser<FunctionSyntax> FunctionRaw =>
            from name in IdentifierToken.Token().Named("caller_name")
            from args in EmptyArgs().Or(ArgsParameters().Token()).Token().Named("call_statament")
            select new FunctionSyntax
            {
                Identifier = name,
                Parameters = args.Dry()
            };

        public static Parser<string> DecimalToken =>
            Parse.Regex(@"\-?[0-9]{1,}(\.[0-9E\-\+]{1,})?", "a number").Token();

        public static Parser<IEnumerable<string>> DecimalArray =>
            from v0 in DecimalToken
            from _0 in Parse.Char(',').Token()
            from v1 in DecimalToken
            from _1 in Parse.Char(',').Token()
            from v2 in DecimalToken
            from _2 in Parse.Char(',').Token()
            from v3 in DecimalToken
            from _3 in Parse.Char(',').Token()
            from v4 in DecimalToken
            from _4 in Parse.Char(',').Token()
            from v5 in DecimalToken
            select new[] {v0, v1, v2, v3, v4, v5};

        public static Parser<PositionSyntax> MoveFunctionRaw =>
            from open in Parse.Char('[').Token()
            from pos in DecimalArray.Token()
            from close in Parse.Char(']').Token()
            from speed in 
                When(Keyword(URScriptWords.Speed), Parse.DecimalInvariant).
                Token().Named("when speed").Optional()
            from acceleration in
                When(Keyword(URScriptWords.Acceleration), Parse.DecimalInvariant).
                Token().Named("when acceleration").Optional()
            from move_type in 
                AsType(URScriptWords.Linear).Or(
                AsType(URScriptWords.Circular)).Or(
                AsType(URScriptWords.Joint)).Optional()
            select new PositionSyntax
            {
                Position = pos,
                Speed = speed.GetOrDefault(),
                Acceleration = acceleration.GetOrDefault(),
                TargetInstruction = move_type.GetOrElse("Joint")
            };
        
        public static Parser<string> When(Parser<string> keyword, Parser<string> value) => 
            Keyword(URScriptWords.When).
                Then(_ => Parse.WhiteSpace).
                Then(_ => keyword).
                Then(_ => Parse.WhiteSpace).
                Then(_ => value);
        
        public static Parser<string> Keyword(string text) =>
            Parse.IgnoreCase(text).Then(n => Parse.LetterOrDigit.Not()).Return(text);
        
        public static Parser<IncludeSyntax> Include =>
            from reg in Parse.Char('@').Token()
            from _ in Keyword(URScriptWords.Include).Token()
            from open in Parse.Char('<').Token()
            from files in FileNameToken.DelimitedBy(Parse.Char(','))
            from close in Parse.Char('>').Token()
            from end in CallEndToken
            from type in AsType(URScriptWords.Fragment)
            select new IncludeSyntax { FilesName = files };

        
        private static Parser<dynamic> CallEndToken => (
                from o in Parse.Char('(')
                from c in Parse.Char(')')
                select new { }).Token().Named("call_end_statament");

        private static Parser<string> AsType(string Type) =>
            from @as in Keyword(URScriptWords.As)
            from _1 in Parse.WhiteSpace
            from type in Keyword(Type).Token()
            from c in Parse.Char(';').Token()
            select type;


        public static Parser<FunctionSyntax> Function =>
            FunctionRaw.
                Token().
                Named("function");
        private static Parser<string> FileNameToken =>
            from _1 in Parse.Char('\'').Token()
            from name in RawIdentifier.Token()
            from _2 in Parse.Char('\'').Token()
            select name;
        public static Parser<PositionSyntax> MoveFunction =>
            MoveFunctionRaw.
                Token().
                Named("move_function");
    }
}

