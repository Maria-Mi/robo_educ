﻿namespace urscript.interactive
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.IO;
    using System.Linq;
    using System.Text;
    using exceptions;
    using MoreLinq;
    using Sprache;
    using Troschuetz.Random;

    public static class URSc
    {
        public static Parser<string> CommentToken => TokenStorage.CommentParser;
        public static Parser<IncludeSyntax> Include => TokenStorage.Include;

        public static bool TryAssembly(SourceFile sourceCode, out string result)
        {
            try
            {
                result = Assembly(sourceCode);
                return true;
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                result = null;
                return false;
            }
        }

        public static string Assembly(string sourceCode) => Assembly(new SourceFileInMemory(sourceCode, "<unknown>"));

        public static string Assembly(SourceFile sourceCode)
        {
            var content = sourceCode.Content;
            var compiledCode = new StringBuilder();
            content = content.Replace("\r", "");
            var lines = ProcessIncludes(content);
            var lineCode = 0;
            foreach (var line in lines)
            {
                lineCode++;
                if(string.IsNullOrEmpty(line))
                    continue;
                if (ParseToken(CommentToken, line, lineCode, out _))
                    continue;
                if (ParseToken(TokenStorage.Function, line, lineCode, out var result1))
                {
                    compiledCode.AppendLine(result1.Construct());
                    continue;
                }
                if (ParseToken(TokenStorage.MoveFunction, line, lineCode, out var result2))
                {
                    compiledCode.AppendLine(result2.Construct());
                    continue;
                }
            }

            var compiledLines = compiledCode.ToString().Replace("\r", "").Split('\n');
            var wrapedCompiledCode = new StringBuilder();
            wrapedCompiledCode.AppendLine("def step_forward():");
            foreach (var compiledLine in compiledLines)
            {
                if(string.IsNullOrEmpty(compiledLine))
                    continue;
                wrapedCompiledCode.AppendLine($"\t{compiledLine}");
            }

            wrapedCompiledCode.AppendLine("end");


            try
            {
                // create output
                if (!Directory.Exists("DB/temp"))
                    Directory.CreateDirectory("DB/temp");
                File.WriteAllText($"db/temp/{Guid.NewGuid().ToString().Split('-').First()}.{sourceCode.Name}.fragment.o", wrapedCompiledCode.ToString());
            }
            catch { }
            return wrapedCompiledCode.ToString();
        }

        private static bool ParseToken<R>(Parser<R> token, string line, int linec, out R result)
        {
            var res = token.TryParse(line);

            if (res.WasSuccessful)
            {
                result = res.Value;
                return true;
            }
            if(res.Remainder.Column > 1)
                throw new CompilationFailedException(
                    $"UR0020: {res.Message} at {linec} line {res.Remainder.Column} column", 
                    $"{res.Remainder.Source} ...",
                    $"{new string(' ', res.Remainder.Column - 1)}^");
            result = default(R);
            return false;
        }

        public static string[] ProcessIncludes(string sourceCode, List<string> modules = null)
        {
            if(modules == null) modules = new List<string>();

            var source = sourceCode;
            var lines = sourceCode.Split('\n');
            var lineCount = 0;
            foreach (var line in lines)
            {
                lineCount++;
                if (ParseToken(CommentToken, line, lineCount, out _))
                    continue;
                if (!ParseToken(Include, line, lineCount, out var include))
                    continue;
                foreach (var name in include.FilesName)
                {
                    // not working
                    //if (modules.Contains(name))
                    //    throw new InfusedModuleRecursionException(name);
                    modules.Add(name);
                }
                var includingFile = ResolveModule(include);
                var sourceModule = SourceManager.InvokeModule(includingFile);
                source = source.Replace(line, sourceModule.Content);
            }
            
            return 
                IsContainsIncludes(source) ? ProcessIncludes(source, modules) : 
                source.Split('\n');
        }

        private static bool IsContainsIncludes(string source)
        {
            var lines = source.Split('\n');
            var linec = 0;
            foreach (var line in lines)
            {
                linec++;
                if(string.IsNullOrEmpty(line))
                    continue;
                if(ParseToken(CommentToken, line, linec, out var _))
                    continue;
                if (ParseToken(Include, line, linec, out var _))
                    return true;
            }

            return false;
        }

        private static string ResolveModule(IncludeSyntax include)
        {
            return include.FilesName.Count() > 1 ?
                        include.FilesName.ToList()[new TRandom().Next(0, include.FilesName.Count() - 1)] :
                        include.FilesName.First();
        }
    }
}