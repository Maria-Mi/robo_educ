﻿namespace urscript.interactive
{
    using System.Collections.Generic;
    using Sprache;

    public interface IResult
    {
        /// <summary>
        /// Gets a value indicating whether wether parsing was successful.
        /// </summary>
        bool WasSuccessful { get; }
        /// <summary>
        /// Gets the error message.
        /// </summary>
        string Message { get; }
        /// <summary>
        /// Gets the parser expectations in case of error.
        /// </summary>
        IEnumerable<string> Expectations { get; }
        /// <summary>
        /// Gets the remainder of the input.
        /// </summary>
        IInput Remainder { get; }
    }

    public static class ResultEx
    {
        public static IResult ToAnonimusResult<R>(this IResult<R> r)
        {
            return new Result
            {
                Expectations = r.Expectations,
                Message = r.Message,
                Remainder = r.Remainder,
                WasSuccessful = r.WasSuccessful
            };
        }

    }

    public class Result : IResult
    {
        public bool WasSuccessful { get; set; }
        public string Message { get; set; }
        public IEnumerable<string> Expectations { get; set; }
        public IInput Remainder { get; set; }
    }
}