## UR Phoenix API

### Запрос на исполнение команды (выполнение напитка)

```typescript
post /api/v1/command

запрос:
{ name: string }


результат:

{ status: number }
```

значение статусов:

```YAML
file_not_found: -2
script_execute_fault: -3
invalid_request: -4
compilation_error: -19
ok: 0
robot_busy: 14
driver_is_not_nonnected_to_controller: 247
```

Когда статус 0, показываем рецепт   
Если статус не 0, показываем ошибку робота

### Long Pulling (запрос статуса)

```typescript
get /api/v1/status?key=NAME_DRINK


результат:

{ status: number }
```

значение статусов:

```YAML
emergency_or_sec_fault: -1 # аварийная остановка или возникновение коллизии (показываем экран ошибки)
script_has_complete: 1 # показываем экран "все готово, извольте нажратца" 
script_work: 2 # ждем
```
