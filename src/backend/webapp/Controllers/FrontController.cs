﻿namespace backend.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Drawing;
    using Microsoft.AspNetCore.Mvc;
    using Newtonsoft.Json;
    using System.IO;
    using System.Linq;
    using System.Management.Automation;
    using System.Management.Automation.Runspaces;
    using System.Threading;
    using RC.Framework.Screens;
    using ur5.core;
    using ur5.models;
    using ur5.network;
    using urscript.interactive;
    using urscript.interactive.exceptions;
    using RuntimeException = urscript.interactive.RuntimeException;
    using file = System.IO.File;

    [Route("api/v1")]
    [ApiController]
    public class FrontController : ControllerBase
    {
        private void RunPs1(string script)
        {
            try
            {
                using (PowerShell PowerShellInstance = PowerShell.Create())
                {
                    Screen.WriteLine($"Execute posthook '{Path.GetFileName(script)}'".To(Color.Gray));
                    PowerShellInstance.AddScript(file.ReadAllText(script));
                    PowerShellInstance.Invoke();
                    Thread.Sleep(200);
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }
        [Route("command")]
        [HttpPost]
        public IActionResult Command(ProgramCommand model)
        {
            if(model == null || string.IsNullOrEmpty(model.Name))
                return HasOk(new CommandResponce { status = -4 }); // invalid request
            if(!file.Exists($"./db/fragments/{model.Name.ToLower()}.frag"))
                return HasOk(new CommandResponce { status = -2 }); // file not found
            if (!MotherBoard.Call<HightLevelSocket>("socket").IsConnected())
                return HasOk(new CommandResponce { status = 247 }); // driver is not connected to urcontroller
            if (MotherBoard.CurrentState != DashBoardState.STOPPED)
                return HasOk(new CommandResponce { status = 14 }); // robot busy
            var finalCode = "";

            try
            {
                finalCode = URSc.Assembly(SourceManager.InvokeModule(model.Name.ToLower()));
            }
            catch (CompilationFailedException e)
            {
                Screen.WriteLine($"{e}".To(Color.Red));
                return HasOk(new CommandResponce {status = -19});
            }
            catch (RuntimeException e)
            {
                Screen.WriteLine($"{e}".To(Color.Yellow));
                return HasOk(new CommandResponce {status = -19});
            }
            MotherBoard.SetNewProgram(new Program(model.Name, Guid.NewGuid().ToString(), new SourceFileInMemory(finalCode, model.Name)));
            var result = MotherBoard.Play();
            if (result == MotherBoard.ScriptStatus.FAUL)
                return HasOk(new CommandResponce {status = -3});

            if (file.Exists($"./db/posthook/{model.Name}.ps1"))
                RunPs1($"./db/posthook/{model.Name}.ps1");
            Screen.WriteLine($"Succes compile '{model.Name}' module.".To(Color.Yellow));
            return HasOk(new CommandResponce { status = 0 });
        }

        [HttpGet]
        [Route("status")]
        public IActionResult Status([FromQuery] string key)
        {
            var prog = MotherBoard.ProgramStack.FirstOrDefault(x => x.Name == key);

            OkObjectResult Failed() => Ok(new { status = -1 });
            OkObjectResult Okay  () => Ok(new { status =  1 });
            OkObjectResult Wait  () => Ok(new { status =  2 });


            if (prog == null)
            {
                Command(new ProgramCommand {Name = key});
                prog = MotherBoard.ProgramStack.FirstOrDefault(x => x.Name == key);
            }
            Thread.Sleep(200);

            if (prog == null)
                return Failed();

            switch (prog.Status)
            {
                case ProgramStatus.Paused:
                case ProgramStatus.NonStarted:
                case ProgramStatus.Playing:
                    return Wait();
                case ProgramStatus.FailedToPlay:
                case ProgramStatus.UnknownError:
                    MotherBoard.ProgramStack.RemoveWhere(x => x.Name == key);
                    return Failed();
                case ProgramStatus.Ended:
                    MotherBoard.ProgramStack.RemoveWhere(x => x.Name == key);
                    return Okay();
            }

            MotherBoard.ProgramStack.RemoveWhere(x => x.Name == key);
            return Failed();
        }

        public OkObjectResult HasOk(CommandResponce value)
        {
            switch (value.status)
            {
                case -1:
                case 0:
                    break;
                case -2:
                    Screen.WriteLine($"[UR]: {value.status} {"Fragment file not found.".To(Color.Orange)}");
                    break;
                case -3:
                    Screen.WriteLine($"[UR]: {value.status} {"MotherBoard has busy.".To(Color.Orange)}");
                    break;
                case -4:
                    Screen.WriteLine($"[UR]: {value.status} {"invalid request".To(Color.Orange)}");
                    break;
                case -19:
                    Screen.WriteLine($"[UR]: {value.status} {"fragment compilation error".To(Color.Orange)}");
                    break;
                case 14:
                    Screen.WriteLine($"[UR]: {value.status} {"robot has busy".To(Color.Orange)}");
                    break;
                case 247:
                    Screen.WriteLine($"[UR]: {value.status} {"driver is not connected to urcontroller".To(Color.Orange)}");
                    break;
            }
            return Ok(value);
;        }
    }

    public class CommandResponce
    {
        public int status { get; set; }
    }
    public class ProgramCommand
    {
        [JsonProperty("name")]
        public string Name { get; set; }
    }
}