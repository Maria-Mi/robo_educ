﻿namespace backend.Models
{
    public class ActionModel
    {
        public ActionType Type { get; set; }
        public string Value { get; set; }
    }

    public enum ActionType
    {
        Joint,
        Tool
    }
}