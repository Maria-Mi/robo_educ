﻿namespace ur5.network
{
    using System.IO;
    using System.Linq;

    public class URBuffer
    {
        protected readonly MemoryStream nMStream;
        protected URBuffer(byte[] bytes)
        {
            nMStream = new MemoryStream(bytes);
        }
        protected URBuffer()
        {
            nMStream = new MemoryStream();
        }
        public long Rem => nMStream.Length - nMStream.Position;
        public byte[] rem_arr => nMStream.ToArray().Skip((int)nMStream.Position).ToArray();

        protected string dd_str => string.Join("|", nMStream.ToArray().Select(x => $"0x{x:X2}"));
        protected string dd_str_rem => string.Join("|", rem_arr.Select(x => $"0x{x:X2}"));

        public static IURBufferReader Create(byte[] buffer) => new URBufferReader(buffer);
    }
}