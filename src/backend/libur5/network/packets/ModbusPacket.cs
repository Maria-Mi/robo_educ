﻿namespace ur5.network.packets
{
    using System;
    using core;
    using etc;
    using models;

    public class ModbusPacket : BasePacket
    {
        #region Data

        private bool messageHasError = false;
        private int byteCount = 5;
        private int signalsCount;
        #endregion


        public ModbusPacket(IURBufferReader stream, PacketState box) : base(stream, box) { }

        protected override void ReadImpl(IURBufferReader buffer)
        {
            if (byteCount + 4 <= getPacketState().CurrentMessageLength)
            {
                signalsCount = buffer.readInt32();
                byteCount += 4;
                if (signalsCount > 200)
                {
                    LibLog.Error(
                        $"Modbus error -1: We received a modbus message but signalsCount = {signalsCount} which must mean that data is corrupted", this);
                    messageHasError = true;
                }
            }
            else
            {
                LibLog.Error("Modbus error 0: We received a modbus message but it did not contain a signals count variable", this);
                messageHasError = true;
            }
            if (messageHasError)
                LibLog.Error("Modbus error 8: Could not extract modbus information from controller message", this);
            else
            {
                var msg = new ModbusInfoMessage();
                if (signalsCount <= 0) return;

                for (var i = 0; i < signalsCount; ++i)
                {
                    if (byteCount + 4 >= getPacketState().CurrentMessageLength)
                    {
                        LibLog.Error(
                            $"Modbus error 2: Could not read signal name length of signal number: {i}. Total number of signals = {signalsCount}");
                        messageHasError = true;
                        break;
                    }

                    var signalNameLength = buffer.readInt32();
                    byteCount += 4;
                    if (Math.Abs(signalNameLength) > 20)
                    {
                        LibLog.Error("Modbus error 1: signalNameLength > 20: signalNameLength = " + signalNameLength + ", signal number " + i + " in message");
                        messageHasError = true;
                        break;
                    }

                    var signalNameChars = new char[signalNameLength];

                    bool readSignals()
                    {
                        for (var j = 0; j < signalNameLength; ++j)
                        {
                            ++byteCount;
                            if (byteCount >= getPacketState().CurrentMessageLength)
                            {
                                LibLog.Error(
                                    $"Modbus error 3: Could not read character j = {j} of signalNameLength = {signalNameLength}. Total number of signals = {signalsCount}");
                                messageHasError = true;
                                return false;
                            }

                            signalNameChars[j] = (char)buffer.readByte();
                        }
                        return true;
                    }

                    if (!readSignals())
                        break;

                    var signalName = new string(signalNameChars);
                    if (byteCount + 8 > getPacketState().CurrentMessageLength)
                    {
                        LibLog.Error("Modbus error 4: Could not read signal status of signal i=" + i + ", name=" + signalName + ", sigcount = " + signalsCount);
                        messageHasError = true;
                        break;
                    }

                    byteCount += 8;
                    var signalStatusInt = buffer.readInt16() & 0xffff;
                    var signalResponseTimeInMs = buffer.readInt16() & 0xffff;
                    i = buffer.readInt16() & '\uffff';
                    int signalConnectionStatus = buffer.readInt16();
                    if (string.IsNullOrEmpty(signalName) || string.IsNullOrWhiteSpace(signalName))
                        signalName = null;
                    msg.AddSignal(signalName, signalStatusInt, signalResponseTimeInMs, i, signalConnectionStatus);
                }
                var lenPkg = getPacketState().CurrentMessageLength;
                if (byteCount == lenPkg && !messageHasError)
                    Modbus.NotifityAll(msg);
                else if ((byteCount > lenPkg || byteCount < lenPkg) && !messageHasError)
                    LibLog.Error(
                        $"Modbus error 5: Bytes read does not match message length, message = {msg}, signalsCount = {signalsCount}, messageLength = {getPacketState().CurrentMessageLength}, byteCount = {byteCount}", this);
                else if (messageHasError)
                    LibLog.Error("Modbus error 6: An error was detected during modbus message parsing", this);
            }

            if (byteCount < getPacketState().CurrentMessageLength)
            {
                LibLog.Error(
                    $"Modbus error 9: too few bytes read, signalsCount = {signalsCount} messageLength = {getPacketState().CurrentMessageLength}, byteCount = {byteCount}", this);
                LibLog.Error("Reading rest of message...", this);

                while (true)
                {
                    ++byteCount;
                    if (byteCount > getPacketState().CurrentMessageLength)
                    {
                        LibLog.Error(" Reading rest of message done", this);
                        break;
                    }
                    buffer.readByte();
                }
            }
        }

        protected override void ExecuteImpl(UR5Info info)
        {
            
        }
    }
}