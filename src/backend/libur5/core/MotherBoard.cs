﻿namespace ur5.core
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading;
    using etc;
    using models;
    using network;

    public static class MotherBoard
    {
        public static HashSet<Program> ProgramStack = new HashSet<Program>();
        public static string defaultSpeed { get; set; } = "0.900";
        public static string defaultAcceleration { get; set; } = "0.700";

        public static readonly object syncGuarder = new object();
        public static DashBoardState userProgramState { get; set; }
        public static DashBoardState CurrentState
        {
            get => boardState;
            set
            {
                if(boardState != value)
                LibLog.Trace($"MotherBoard from {boardState} to {value}");
                if (boardState != value)
                {
                    switch (value)
                    {
                        case DashBoardState.PLAYING:
                            playTime = DateTime.UtcNow.Ticks;
                            break;
                        case DashBoardState.PAUSED:
                            program?.Pause();
                            break;
                        case DashBoardState.STOPPED:
                            program?.Stop();
                            break;
                    }
                }
                if ((value == DashBoardState.STOPPED || value == DashBoardState.PAUSED) && userProgramState == DashBoardState.PLAYING)
                    userProgramState = value;
                boardState = value;
            }
        }
        public static GlobalState globalState { get; set; } = GlobalState.OK;
        public static UR5Info LastActualState { get; set; }
        public static void updateState(UR5Info state)
        {
            lock (syncGuarder)
            {
                var t = DateTime.UtcNow.Ticks;

                if (state.IsProgramWork && (!state.IsProgramPaused || state.SpeedFaction > 0.0D))
                    CurrentState = DashBoardState.PLAYING;
                else if(state.IsProgramPaused)
                    CurrentState = DashBoardState.PAUSED;
                else if (!state.IsProgramPaused && state.IsProgramWork)
                    CurrentState = DashBoardState.STOPPED;
                else if(!state.IsProgramWork && t - playTime > 2000)
                    CurrentState = DashBoardState.STOPPED;
                interleaveCounter++;
                LastActualState = state;
            }
        }

        private static void PlayScriptCode(double automaticMoveEpsilon, string code)
        {
            socket.Send(code);
        }
        public enum ScriptStatus
        {
            NONE,
            FAUL,
            OK
        }
        public static ScriptStatus Play(double automaticMoveEpsilon = 1.0)
        {
            if (program == null)
                return ScriptStatus.FAUL;
            if (LastActualState.IsReal && LastActualState.Mode != 0)
            {
                if (LastActualState.Mode != RobotMode.SECURITY_STOPPED)
                {
                    URDialog.BadRobotStateDialog($"{LastActualState.Mode}");
                    globalState = GlobalState.OK;
                    return ScriptStatus.FAUL;
                }
                URDialog.SecurityStoppedDialog();
                return ScriptStatus.FAUL;
            }
            switch (CurrentState)
            {
                case DashBoardState.STOPPED:
                    program.Start();
                    PlayScriptCode(automaticMoveEpsilon, program.Source.Content);
                    return ScriptStatus.OK;
                case DashBoardState.PLAYING:
                    Pause();
                    break;
                default:
                    if (CurrentState == DashBoardState.PLAYING)
                        Pause();
                    else if (CurrentState == DashBoardState.PAUSED)
                        Resume();
                    break;
            }
            return ScriptStatus.FAUL;
        }

        public static string getUIDCurrentProgram() => programUID;

        public static void Stop()
        {
            program?.Stop();
            socket.StopProgram();
            CurrentState = (DashBoardState.STOPPED);
        }
        
        public static void Pause()
        {
            program.Pause();
            socket.Pause();
            CurrentState = (DashBoardState.PAUSED);
        }

        public static void Resume()
        {
            program.Resume();
            socket.Resume();
            CurrentState = (DashBoardState.PLAYING);
        }


        public static void Freedrive()
        {
            if (globalState == GlobalState.FREEDRIVE)
            {
                socket.RunMode();
                globalState = GlobalState.OK;
            }
            else if (globalState == GlobalState.OK)
            {
                socket.FreeDrive();
                globalState = GlobalState.FREEDRIVE;
            }
        }

        public static void SetNewProgram(Program prog)
        {
            ProgramStack.Add(prog);
            programUID = prog.UID;
        }


        #region private
        private static string programUID { get; set; }
        private static long playTime { get; set; }
        private static long interleaveCounter { get; set; }
        private static Program program => ProgramStack.FirstOrDefault(x => x.UID == programUID);
        private static DashBoardState boardState { get; set; } = DashBoardState.UNKNOWN;
        private static HightLevelSocket socket { get; set; }
        private static Thread thread { get; set; }
        #endregion

        #region Public

        public static int Version { get; set; }
        public static int ControllerBoxType { get; set; }
        public static int RobotType { get; set; }
        public static int RobotSubType { get; set; }

        #endregion

        public static T Call<T>(string name)
        {
            Q cast<Q>(object value) => (Q)value;

            switch (name)
            {
                case nameof(playTime):          return cast<T>(playTime);
                case nameof(thread):            return cast<T>(thread);
                case nameof(socket):            return cast<T>(socket);
                case nameof(boardState):        return cast<T>(boardState);
                case nameof(program):           return cast<T>(program);
                case nameof(interleaveCounter): return cast<T>(interleaveCounter);

                default: return default;
            }
        }

        public static HightLevelSocket CreateSocket(string ip, int port)
        {
            if (socket != null)
                return socket;
            socket = new HightLevelSocket(ip, port);
            LibLog.Info($"Success connected on {ip}:{port}");
            StartReceive();
            return socket;
        }

        private static void StartReceive()
        {
            if(thread != null)
                return;
            thread = new Thread(() =>
            {
                while (true)
                {
                    Thread.Sleep(10);
                    socket.ProcessPacket(socket.ReceiveBytes());
                }

            });
            thread.Start();
        }

        public static void ShutdownDriver() => thread?.Abort();
    }

    public enum GlobalState
    {
        /// <summary>
        /// Motherboard is OK
        /// </summary>
        OK,
        /// <summary>
        /// Need re-init robot
        /// </summary>
        NEED_REINIT_CONTROLLER,
        /// <summary>
        /// Mode is freedrive (teach)
        /// </summary>
        FREEDRIVE,
        /// <summary>
        /// Emergency stop button has pressed
        /// </summary>
        EVERGENCY_STOP,
        /// <summary>
        /// Violation security stopped
        /// </summary>
        SECURITY_STOP
    }
    public enum DashBoardState
    {
        UNKNOWN,
        PLAYING,
        PAUSED,
        STOPPED
    }
}